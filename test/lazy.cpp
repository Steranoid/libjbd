#include <gtest/gtest.h>
#include <jbd/lazy>
#include <jbd/ref>

using namespace jbd;

TEST(lazy, construct) {
	auto foo   = []() noexcept -> int { return 3; };
	auto value = lazy{foo};
	static_assert(noexcept(lazy{foo}));
	ASSERT_EQ(value.value(), 3);
	static_assert(noexcept(value.value()));
	static_assert(std::is_same_v<decltype(value.value()), int>);
}

TEST(lazy, deref) {
	auto value = lazy{[]() noexcept {
		return 3;
	}};
	ASSERT_EQ(*value, 3);
	static_assert(noexcept(*value));
	static_assert(std::is_same_v<decltype(*value), int>);
}

TEST(lazy, destruct) {
	auto value = lazy{[] {
		return std::string{"foo"};
	}};
	ASSERT_EQ(value->size(), 3);
	static_assert(std::is_same_v<decltype(*value), std::string const&>);
	static_assert(std::is_same_v<decltype(value.operator->()), std::string const *>);
}

TEST(lazy, cascade_deref) {
	auto foo   = std::string{"foo"};
	auto value = lazy{[ref = jbd::ref{foo}] {
		return ref;
	}};
	ASSERT_EQ(value->size(), 3);
}

TEST(lazy, function_call) {
	auto value = lazy{[]() { return &std::string::size; }};
	ASSERT_EQ(value(std::string{"foo"}), 3);
}