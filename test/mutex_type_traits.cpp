#include <chrono>
#include <jbd/type_traits>
#include <mutex>
#include <shared_mutex>
#include <string>

using namespace jbd;

struct basic_lockable {
	void lock();
	void unlock();
};

struct lockable: virtual basic_lockable {
	auto try_lock() -> bool;
};

struct timed_lockable: virtual lockable {
	template <typename Clock, typename Duration>
	auto try_lock_until(std::chrono::time_point<Clock, Duration> const& timeout)
		-> bool;
	template <typename Rep, typename Period>
	auto try_lock_for(std::chrono::duration<Rep, Period> const& duration) -> bool;
};

struct mutex: virtual lockable {
	mutex() = default;

	mutex(mutex const&)                    = delete;
	auto operator=(mutex const&) -> mutex& = delete;

	mutex(mutex&&)                    = delete;
	auto operator=(mutex&&) -> mutex& = delete;

	~mutex() = default;
};

struct timed_mutex
	: virtual timed_lockable
	, virtual mutex {};

struct shared_lockable {
	void lock_shared();
	void unlock_shared();
	auto try_lock_shared() -> bool;
};
struct shared_timed_lockable: virtual shared_lockable {
	template <typename Clock, typename Duration>
	auto
		try_lock_shared_until(std::chrono::time_point<Clock, Duration> const& timeout)
			-> bool;
	template <typename Rep, typename Period>
	auto try_lock_shared_for(std::chrono::duration<Rep, Period> const& duration)
		-> bool;
};

struct shared_mutex
	: virtual mutex
	, virtual shared_lockable {};

struct shared_timed_mutex
	: virtual timed_mutex
	, virtual shared_mutex
	, virtual shared_timed_lockable {};

static_assert(!is_basic_lockable_v<int>);
static_assert(!is_basic_lockable_v<std::string>);
static_assert(is_basic_lockable_v<std::mutex>);
static_assert(is_basic_lockable_v<std::unique_lock<std::mutex>>);
static_assert(is_basic_lockable_v<std::shared_mutex>);
static_assert(is_basic_lockable_v<std::unique_lock<std::shared_mutex>>);
static_assert(is_basic_lockable_v<std::shared_lock<std::shared_mutex>>);
static_assert(is_basic_lockable_v<std::timed_mutex>);
static_assert(is_basic_lockable_v<std::unique_lock<std::timed_mutex>>);
static_assert(is_basic_lockable_v<std::shared_timed_mutex>);
static_assert(is_basic_lockable_v<std::unique_lock<std::shared_timed_mutex>>);
static_assert(is_basic_lockable_v<std::shared_lock<std::shared_timed_mutex>>);
static_assert(is_basic_lockable_v<basic_lockable>);
static_assert(is_basic_lockable_v<lockable>);
static_assert(is_basic_lockable_v<timed_lockable>);
static_assert(!is_basic_lockable_v<shared_lockable>);
static_assert(!is_basic_lockable_v<shared_timed_lockable>);
static_assert(is_basic_lockable_v<mutex>);
static_assert(is_basic_lockable_v<timed_mutex>);
static_assert(is_basic_lockable_v<shared_mutex>);
static_assert(is_basic_lockable_v<shared_timed_mutex>);

static_assert(!is_lockable_v<int>);
static_assert(!is_lockable_v<std::string>);
static_assert(is_lockable_v<std::mutex>);
static_assert(is_lockable_v<std::unique_lock<std::mutex>>);
static_assert(is_lockable_v<std::shared_mutex>);
static_assert(is_lockable_v<std::unique_lock<std::shared_mutex>>);
static_assert(is_lockable_v<std::shared_lock<std::shared_mutex>>);
static_assert(is_lockable_v<std::timed_mutex>);
static_assert(is_lockable_v<std::unique_lock<std::timed_mutex>>);
static_assert(is_lockable_v<std::shared_timed_mutex>);
static_assert(is_lockable_v<std::unique_lock<std::shared_timed_mutex>>);
static_assert(is_lockable_v<std::shared_lock<std::shared_timed_mutex>>);
static_assert(!is_lockable_v<basic_lockable>);
static_assert(is_lockable_v<lockable>);
static_assert(is_lockable_v<timed_lockable>);
static_assert(!is_lockable_v<shared_lockable>);
static_assert(!is_lockable_v<shared_timed_lockable>);
static_assert(is_lockable_v<mutex>);
static_assert(is_lockable_v<timed_mutex>);
static_assert(is_lockable_v<shared_mutex>);
static_assert(is_lockable_v<shared_timed_mutex>);

static_assert(!is_lockable_for_v<int, std::chrono::seconds>);
static_assert(!is_lockable_for_v<std::string, std::chrono::seconds>);
static_assert(!is_lockable_for_v<std::mutex, std::chrono::seconds>);
static_assert(!is_lockable_for_v<std::shared_mutex, std::chrono::seconds>);
static_assert(is_lockable_for_v<std::timed_mutex, std::chrono::seconds>);
static_assert(is_lockable_for_v<std::unique_lock<std::timed_mutex>, std::chrono::seconds>);
static_assert(is_lockable_for_v<std::shared_timed_mutex, std::chrono::seconds>);
static_assert(is_lockable_for_v<std::unique_lock<std::shared_timed_mutex>, std::chrono::seconds>);
static_assert(is_lockable_for_v<std::shared_lock<std::shared_timed_mutex>, std::chrono::seconds>);
static_assert(!is_lockable_for_v<basic_lockable, std::chrono::seconds>);
static_assert(!is_lockable_for_v<lockable, std::chrono::seconds>);
static_assert(is_lockable_for_v<timed_lockable, std::chrono::seconds>);
static_assert(!is_lockable_for_v<shared_lockable, std::chrono::seconds>);
static_assert(!is_lockable_for_v<shared_timed_lockable, std::chrono::seconds>);
static_assert(!is_lockable_for_v<mutex, std::chrono::seconds>);
static_assert(is_lockable_for_v<timed_mutex, std::chrono::seconds>);
static_assert(!is_lockable_for_v<shared_mutex, std::chrono::seconds>);
static_assert(is_lockable_for_v<shared_timed_mutex, std::chrono::seconds>);

static_assert(!is_lockable_until_v<int, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<std::string, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<std::mutex, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<std::shared_mutex, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<std::timed_mutex, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<std::unique_lock<std::timed_mutex>, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<std::shared_timed_mutex, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<std::unique_lock<std::shared_timed_mutex>, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<std::shared_lock<std::shared_timed_mutex>, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<basic_lockable, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<lockable, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<timed_lockable, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<shared_lockable, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<shared_timed_lockable, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<mutex, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<timed_mutex, std::chrono::system_clock::time_point>);
static_assert(!is_lockable_until_v<shared_mutex, std::chrono::system_clock::time_point>);
static_assert(is_lockable_until_v<shared_timed_mutex, std::chrono::system_clock::time_point>);

static_assert(!is_shared_lockable_for_v<int, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::string, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::mutex, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::unique_lock<std::mutex>, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::shared_mutex, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::unique_lock<std::shared_mutex>, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::shared_lock<std::shared_mutex>, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::timed_mutex, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::unique_lock<std::timed_mutex>, std::chrono::seconds>);
static_assert(is_shared_lockable_for_v<std::shared_timed_mutex, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::unique_lock<std::shared_timed_mutex>, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<std::shared_lock<std::shared_timed_mutex>, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<basic_lockable, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<lockable, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<timed_lockable, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<shared_lockable, std::chrono::seconds>);
static_assert(is_shared_lockable_for_v<shared_timed_lockable, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<mutex, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<timed_mutex, std::chrono::seconds>);
static_assert(!is_shared_lockable_for_v<shared_mutex, std::chrono::seconds>);
static_assert(is_shared_lockable_for_v<shared_timed_mutex, std::chrono::seconds>);

static_assert(!is_shared_lockable_until_v<int, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::string, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::mutex, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::unique_lock<std::mutex>, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::shared_mutex, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::unique_lock<std::shared_mutex>, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::shared_lock<std::shared_mutex>, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::timed_mutex, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::unique_lock<std::timed_mutex>, std::chrono::system_clock::time_point>);
static_assert(is_shared_lockable_until_v<std::shared_timed_mutex, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::unique_lock<std::shared_timed_mutex>, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<std::shared_lock<std::shared_timed_mutex>, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<basic_lockable, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<lockable, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<timed_lockable, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<shared_lockable, std::chrono::system_clock::time_point>);
static_assert(is_shared_lockable_until_v<shared_timed_lockable, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<mutex, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<timed_mutex, std::chrono::system_clock::time_point>);
static_assert(!is_shared_lockable_until_v<shared_mutex, std::chrono::system_clock::time_point>);
static_assert(is_shared_lockable_until_v<shared_timed_mutex, std::chrono::system_clock::time_point>);

static_assert(!is_mutex_v<int>);
static_assert(!is_mutex_v<std::string>);
static_assert(is_mutex_v<std::mutex>);
static_assert(!is_mutex_v<std::unique_lock<std::mutex>>);
static_assert(is_mutex_v<std::shared_mutex>);
static_assert(!is_mutex_v<std::unique_lock<std::shared_mutex>>);
static_assert(!is_mutex_v<std::shared_lock<std::shared_mutex>>);
static_assert(is_mutex_v<std::timed_mutex>);
static_assert(!is_mutex_v<std::unique_lock<std::timed_mutex>>);
static_assert(is_mutex_v<std::shared_timed_mutex>);
static_assert(!is_mutex_v<std::unique_lock<std::shared_timed_mutex>>);
static_assert(!is_mutex_v<std::shared_lock<std::shared_timed_mutex>>);
static_assert(!is_mutex_v<basic_lockable>);
static_assert(!is_mutex_v<lockable>);
static_assert(!is_mutex_v<timed_lockable>);
static_assert(!is_mutex_v<shared_lockable>);
static_assert(!is_mutex_v<shared_timed_lockable>);
static_assert(is_mutex_v<mutex>);
static_assert(is_mutex_v<timed_mutex>);
static_assert(is_mutex_v<shared_mutex>);
static_assert(is_mutex_v<shared_timed_mutex>);

static_assert(!is_shared_mutex_v<int>);
static_assert(!is_shared_mutex_v<std::string>);
static_assert(!is_shared_mutex_v<std::mutex>);
static_assert(!is_shared_mutex_v<std::unique_lock<std::mutex>>);
static_assert(is_shared_mutex_v<std::shared_mutex>);
static_assert(!is_shared_mutex_v<std::unique_lock<std::shared_mutex>>);
static_assert(!is_shared_mutex_v<std::shared_lock<std::shared_mutex>>);
static_assert(!is_shared_mutex_v<std::timed_mutex>);
static_assert(!is_shared_mutex_v<std::unique_lock<std::timed_mutex>>);
static_assert(is_shared_mutex_v<std::shared_timed_mutex>);
static_assert(!is_shared_mutex_v<std::unique_lock<std::shared_timed_mutex>>);
static_assert(!is_shared_mutex_v<std::shared_lock<std::shared_timed_mutex>>);
static_assert(!is_shared_mutex_v<basic_lockable>);
static_assert(!is_shared_mutex_v<lockable>);
static_assert(!is_shared_mutex_v<timed_lockable>);
static_assert(!is_shared_mutex_v<shared_lockable>);
static_assert(!is_shared_mutex_v<shared_timed_lockable>);
static_assert(!is_shared_mutex_v<mutex>);
static_assert(!is_shared_mutex_v<timed_mutex>);
static_assert(is_shared_mutex_v<shared_mutex>);
static_assert(is_shared_mutex_v<shared_timed_mutex>);
