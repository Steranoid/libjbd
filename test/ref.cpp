#include "jbd/bits/deref.hpp"

#include <gtest/gtest.h>
#include <jbd/ref>
#include <utility>

TEST(ref, construct) {
	auto integer   = 3;
	auto reference = jbd::ref{integer};
}

TEST(ref, deref) {
	auto integer   = 3;
	auto reference = jbd::ref{integer};
	ASSERT_EQ(*reference, integer);
}

TEST(ref, destruct) {
	auto value     = std::string{"foo"};
	auto reference = jbd::ref{value};
	ASSERT_EQ(value.size(), reference->size());
}

TEST(ref, cascade_destruct) {
	auto value          = std::string{"foo"};
	auto reference      = jbd::ref{value};
	auto meta_reference = jbd::ref<jbd::ref<std::string>>{reference};

	ASSERT_EQ(value.size(), meta_reference->size());
}

TEST(ref, function_call) {
	auto value     = [](std::string const& value) { return value.size(); };
	auto reference = jbd::ref{value};
	ASSERT_EQ(reference("foo"), 3);
}