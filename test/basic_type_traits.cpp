#include <jbd/ref>
#include <jbd/type_traits>
#include <memory>

using namespace jbd;

struct copyable {};
struct not_copy_constructible {
	not_copy_constructible(not_copy_constructible const& other)                    = delete;
	auto operator=(not_copy_constructible const& other) -> not_copy_constructible& = default;

	not_copy_constructible(not_copy_constructible&& other)                    = default;
	auto operator=(not_copy_constructible&& other) -> not_copy_constructible& = default;

	~not_copy_constructible() = default;
};
struct not_copy_assignable {
	not_copy_assignable(not_copy_assignable const& other)                    = default;
	auto operator=(not_copy_assignable const& other) -> not_copy_assignable& = delete;

	not_copy_assignable(not_copy_assignable&& other)                    = default;
	auto operator=(not_copy_assignable&& other) -> not_copy_assignable& = default;

	~not_copy_assignable() = default;
};
struct not_move_constructible {
	not_move_constructible(not_move_constructible const& other)                    = default;
	auto operator=(not_move_constructible const& other) -> not_move_constructible& = default;

	not_move_constructible(not_move_constructible&& other)                    = delete;
	auto operator=(not_move_constructible&& other) -> not_move_constructible& = default;

	~not_move_constructible();
};
struct not_move_assignable {
	not_move_assignable(not_move_assignable const& other)                    = default;
	auto operator=(not_move_assignable const& other) -> not_move_assignable& = default;

	not_move_assignable(not_move_assignable&& other)                    = default;
	auto operator=(not_move_assignable&& other) -> not_move_assignable& = delete;

	~not_move_assignable();
};

static_assert(is_copyable_v<copyable>);
static_assert(!is_copyable_v<not_copy_constructible>);
static_assert(!is_copyable_v<not_copy_assignable>);
static_assert(is_copyable_v<not_move_constructible>);
static_assert(is_copyable_v<not_move_assignable>);
static_assert(is_movable_v<copyable>);
static_assert(is_movable_v<not_copy_constructible>);
static_assert(is_movable_v<not_copy_assignable>);
static_assert(!is_movable_v<not_move_constructible>);
static_assert(!is_movable_v<not_move_assignable>);

static_assert(!is_struct_dereferenceable_v<int>);
static_assert(is_struct_dereferenceable_v<std::string*>);
static_assert(is_struct_dereferenceable_v<std::unique_ptr<std::string>>);
static_assert(is_struct_dereferenceable_v<std::unique_ptr<std::string> const>);