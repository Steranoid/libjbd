#include <chrono>
#include <future>
#include <gtest/gtest.h>
#include <jbd/mutex>
#include <mutex>
#include <stdexcept>
#include <type_traits>

// NOLINTBEGIN
struct Foo {
	Foo() {}
	Foo(Foo&&) {}
};
// NOLINTEND

using namespace std::chrono_literals;

static_assert(std::is_nothrow_default_constructible_v<jbd::mutex<int>>);
static_assert(!std::is_nothrow_default_constructible_v<jbd::mutex<Foo>>);
static_assert(std::is_constructible_v<jbd::mutex<int>, int>);
static_assert(std::is_nothrow_constructible_v<jbd::mutex<int>, int>);
static_assert(std::is_constructible_v<jbd::mutex<Foo>, Foo>);
static_assert(!std::is_nothrow_constructible_v<jbd::mutex<Foo>, Foo>);

TEST(mutex, construct) {
	auto mutex = jbd::mutex<int>{};
}

TEST(mutex, constructFromValue) {
	[[maybe_unused]] auto mutex = jbd::mutex{4};
}

TEST(mutex, constructAndLockShared) {
	auto mutex = jbd::mutex{std::string{"foo"}};
	auto lock  = mutex.lock();
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, "foo");
}

TEST(mutex, constructAndLock) {
	auto mutex = jbd::mutex{std::string{"foo"}};
	auto lock  = mutex.lock();
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, "foo");
}

TEST(mutex, constLock) {
	auto const mutex = jbd::mutex<std::string>{"foo"};
	auto       lock  = mutex.lock();
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, "foo");
}

TEST(mutex, tryLockUnused) {
	auto mutex = jbd::mutex<std::string>{"foo"};
	auto lock  = mutex.try_lock();
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, "foo");
}

TEST(mutex, tryLockUsed) {
	auto mutex = jbd::mutex<std::string>{"foo"};
	auto lock  = mutex.lock();
	ASSERT_TRUE(lock);
	auto try_lock = std::async([&mutex]() -> bool {
		auto lock = mutex.try_lock();
		return lock.owns_lock();
	});
	ASSERT_FALSE(try_lock.get());
}

TEST(mutex, tryLockUsedShared) {
	auto mutex = jbd::shared_mutex<std::string>{"foo"};
	auto lock  = mutex.const_try_lock();
	auto lock2 = mutex.const_try_lock();
	auto lock3 = mutex.try_lock();
	ASSERT_TRUE(lock);
	ASSERT_TRUE(lock2);
	ASSERT_FALSE(lock3);
}

TEST(mutex, timedMutexLockFor) {
	auto mutex = jbd::timed_mutex<std::string>{"foo"};
	auto lock  = mutex.try_lock_for(std::chrono::seconds{4});
	ASSERT_EQ(*lock, "foo");
}

TEST(mutex, timedMutexLockUntil) {
	auto mutex = jbd::timed_mutex<std::string>{"foo"};
	auto lock =
		mutex.try_lock_until(std::chrono::system_clock::now() + std::chrono::seconds{4});
	ASSERT_EQ(*lock, "foo");
}

TEST(mutex, function) {
	auto mutex = jbd::mutex{[]() { return std::string{"foo"}; }};
	auto lock  = mutex.lock();
	ASSERT_EQ(lock(), "foo");
}