#include <gtest/gtest.h>
#include <jbd/memory>
#include <string>

TEST(box, construct) {
	auto value = jbd::box{1};
}

TEST(box, deref) {
	auto value = jbd::box{1};
	ASSERT_EQ(*value, 1);
}

TEST(box, deref_struct) {
	auto value = jbd::box{std::string{"foo"}};
	ASSERT_EQ(*value, "foo");
	ASSERT_EQ(value->size(), 3);
}

TEST(box, function) {
	auto value = jbd::box{[]() { return std::string{"foo"}; }};
	ASSERT_EQ(value(), "foo");
}

TEST(box, cascade_deref) {
	auto value = jbd::box{jbd::option{std::string{"foo"}}};
	ASSERT_EQ(**value, "foo");
	ASSERT_TRUE(*value);
	ASSERT_EQ(value->size(), 3);
}

static_assert(sizeof(jbd::option<jbd::box<int>>) == sizeof(jbd::box<int>));

TEST(box, option_box) {
	auto value = jbd::option{jbd::box{std::string{"foo"}}};
	ASSERT_TRUE(value);
	ASSERT_EQ(**value, "foo");
	ASSERT_EQ(value->size(), 3);

	auto moved_value = std::move(value);
	ASSERT_TRUE(moved_value);

	moved_value = jbd::none;
	ASSERT_FALSE(moved_value);
}