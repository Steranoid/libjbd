#include <gtest/gtest.h>
#include <jbd/option>
#include <utility>

TEST(option, construct_none) {
	auto opt = jbd::option<int>{};
	ASSERT_FALSE(opt);
}

TEST(option, construct_some) {
	auto opt = jbd::option{std::string{"foo"}};
	ASSERT_TRUE(opt);
	ASSERT_EQ(*opt, "foo");
	ASSERT_EQ(opt->size(), 3);
}

TEST(option, copy) {
	auto opt  = jbd::option{std::string{"foo"}};
	auto opt2 = opt;
	ASSERT_TRUE(opt);
	ASSERT_EQ(*opt, "foo");
	ASSERT_EQ(opt->size(), 3);
	ASSERT_TRUE(opt2);
	ASSERT_EQ(*opt2, "foo");
	ASSERT_EQ(opt2->size(), 3);
}

TEST(option, copy_assign) {
	auto opt  = jbd::option<std::string>{};
	auto opt2 = jbd::option{std::string{"foo"}};
	opt       = opt2;
	ASSERT_TRUE(opt);
	ASSERT_EQ(*opt, "foo");
	ASSERT_EQ(opt->size(), 3);
	ASSERT_TRUE(opt2);
	ASSERT_EQ(*opt2, "foo");
	ASSERT_EQ(opt2->size(), 3);
}

TEST(option, move) {
	auto opt  = jbd::option{std::string{"foo"}};
	auto opt2 = std::move(opt);
	ASSERT_TRUE(opt2);
	ASSERT_EQ(*opt2, "foo");
	ASSERT_EQ(opt2->size(), 3);
}

TEST(option, move_assign) {
	auto opt  = jbd::option<std::string>{};
	auto opt2 = jbd::option{std::string{"foo"}};
	opt       = std::move(opt2);
	ASSERT_TRUE(opt);
	ASSERT_EQ(*opt, "foo");
	ASSERT_EQ(opt->size(), 3);
}

TEST(option, test) {
	auto opt = jbd::option{1};
	ASSERT_TRUE(opt);
	ASSERT_EQ(*opt, 1);
	opt = jbd::none;
	ASSERT_FALSE(opt);
}

TEST(option, struct_deref) {
	auto opt = jbd::option{std::string{"foo"}};
	ASSERT_TRUE(opt);
	ASSERT_EQ(*opt, "foo");
	ASSERT_EQ(opt->size(), 3);
}

TEST(option, cascade_deref) {
	auto opt = jbd::option<jbd::option<std::string>>{jbd::option{std::string{"foo"}}};
	ASSERT_TRUE(opt);
	ASSERT_EQ(**opt, "foo");
	ASSERT_EQ(opt->size(), 3);
	ASSERT_FALSE(opt->empty());
}

static_assert(sizeof(jbd::option<jbd::ref<int>>) == sizeof(jbd::ref<int>));

TEST(option, ref) {
	auto value = 3;
	auto opt   = jbd::option{value};
	ASSERT_EQ(*opt, value);
	value = 3;
	ASSERT_EQ(*opt, value);
}

TEST(option, struct_deref_ref) {
	auto value = std::string{"foo"};
	auto opt   = jbd::option{value};
	ASSERT_EQ(*opt, value);
	ASSERT_EQ(*std::as_const(opt), value);
	ASSERT_EQ(opt->size(), 3);
	value = "bar";
	ASSERT_EQ(*opt, value);
	ASSERT_EQ(opt->size(), 3);
}

TEST(option, eq) {
	ASSERT_EQ(jbd::option{1}, jbd::option{1});
	ASSERT_EQ(jbd::option{1}, 1);
	ASSERT_EQ(1, jbd::option{1});
	ASSERT_NE(jbd::option{1}, jbd::none);
	ASSERT_NE(jbd::none, jbd::option{1});
}