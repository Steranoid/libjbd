#include "jbd/bits/is_template.hpp"

#include <jbd/type_traits>
#include <string>
#include <vector>

static_assert(jbd::is_template_v<std::basic_string, std::string>);
static_assert(jbd::is_template_v<std::vector, std::vector<int>>);
static_assert(!jbd::is_template_v<std::basic_string, int>);