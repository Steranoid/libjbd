#include <functional>
#include <gtest/gtest.h>
#include <jbd/mutex>
#include <jbd/type_traits>
#include <mutex>
#include <stdexcept>
#include <system_error>
#include <tuple>
#include <type_traits>

using namespace jbd;

static_assert(is_struct_dereferenceable_v<value_lock<std::string, std::unique_lock<std::mutex>>>);

TEST(value_lock, no_lock) {
	auto value = 4;
	auto lock  = value_lock<int, std::unique_lock<std::mutex>>{value};
	ASSERT_FALSE(lock);
	ASSERT_THROW(std::ignore = *lock, std::runtime_error);
}

TEST(value_lock, unique_lock) {
	auto mutex = std::mutex{};
	auto value = 4;
	auto lock  = value_lock{value, std::unique_lock{mutex}};
	ASSERT_FALSE(mutex.try_lock());
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, 4);
}

TEST(value_lock, shared_lock) {
	auto mutex = std::shared_mutex{};
	auto value = 4;
	auto lock  = value_lock{value, std::shared_lock{mutex}};
	ASSERT_FALSE(mutex.try_lock());
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, 4);
}

TEST(value_lock, mutex) {
	auto mutex = std::mutex{};
	auto value = 4;
	auto lock  = value_lock{value, mutex};
	ASSERT_FALSE(mutex.try_lock());
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, 4);
}

TEST(value_lock, defer_lock) {
	auto mutex = std::mutex{};
	auto value = 4;
	auto lock  = value_lock{value, mutex, std::defer_lock};
	ASSERT_TRUE(mutex.try_lock());
	ASSERT_FALSE(lock);
	mutex.unlock();
	ASSERT_EQ(*lock, 4);
}

TEST(value_lock, try_to_lock) {
	auto mutex = std::mutex{};
	auto value = 4;
	auto lock  = value_lock{value, mutex, std::try_to_lock};
	ASSERT_FALSE(mutex.try_lock());
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, 4);
}

TEST(value_lock, adopt_lock) {
	auto mutex  = std::mutex{};
	auto mutex2 = std::mutex{};
	auto value  = 4;
	auto copy   = 3;

	{
		std::lock(mutex, mutex2);
		auto lock  = value_lock{value, mutex, std::adopt_lock};
		auto lock2 = value_lock{copy, mutex2, std::adopt_lock};
		ASSERT_FALSE(mutex.try_lock());
		ASSERT_FALSE(mutex2.try_lock());
		ASSERT_TRUE(lock);
		ASSERT_EQ(*lock, 4);
		ASSERT_TRUE(lock2);
		ASSERT_EQ(*lock2, 3);
	}

	auto lock  = value_lock{value, mutex};
	auto lock2 = value_lock{copy, mutex2};
	ASSERT_TRUE(lock);
	ASSERT_TRUE(lock2);
}

TEST(value_lock, shared_mutex) {
	auto mutex = std::shared_mutex{};
	auto value = 4;
	auto lock  = value_lock{value, mutex};
	ASSERT_FALSE(mutex.try_lock());
	ASSERT_TRUE(lock);
	ASSERT_EQ(*lock, 4);
}

TEST(value_lock, object_accesses) {
	auto mutex = std::mutex{};
	auto str   = std::string{"foo"};
	auto lock  = value_lock{str, mutex};
	ASSERT_FALSE(mutex.try_lock());
	ASSERT_TRUE(lock);
	static_assert(std::is_same_v<decltype(*lock), std::string&>);
	ASSERT_EQ(*lock, str);
	*lock = "bar";
	ASSERT_EQ(*lock, str);
	ASSERT_EQ(lock->size(), str.size());
}

TEST(value_lock, move) {
	auto mutex = std::mutex{};
	auto value = std::string{"foo"};
	auto lock  = value_lock{value, mutex};
	ASSERT_TRUE(lock);
	auto new_lock = std::move(lock);
	ASSERT_TRUE(new_lock);
	ASSERT_FALSE(lock);                                   // NOLINT(bugprone-use-after-move,hicpp-invalid-access-moved)
	ASSERT_THROW(std::ignore = *lock, std::system_error); // NOLINT(bugprone-use-after-move,hicpp-invalid-access-moved,clang-analyzer-cplusplus.Move)
}

TEST(value_lock, cascade_deref) {
	auto value = std::string{"foo"};
	auto ref   = jbd::ref{value};
	auto mutex = std::mutex{};
	auto lock  = value_lock{ref, mutex};
	ASSERT_EQ(lock->size(), 3);
}

TEST(value_lock, function) {
	auto mutex  = std::mutex{};
	auto value  = std::string{"foo"};
	auto lambda = [&]() -> std::string const& { return value; };
	auto lock   = value_lock{lambda, mutex};
	ASSERT_EQ(lock(), value);
}