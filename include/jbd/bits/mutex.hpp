#pragma once

#include "jbd/bits/value_lock.hpp"

#include <jbd/type_traits>
#include <mutex>
#include <shared_mutex>
#include <type_traits>

namespace jbd {
template <typename T, typename M>
class base_mutex {
public:
	static_assert(!std::is_reference_v<T>);
	static_assert(!std::is_const_v<T>);
	static_assert(is_mutex_v<M>);

	using value_type            = T;
	using mutex_type            = M;
	using lock_type             = value_lock<value_type, std::unique_lock<mutex_type>>;
	using inner_const_lock_type = std::conditional_t<
		is_shared_mutex_v<mutex_type>,
		std::shared_lock<mutex_type>,
		std::unique_lock<mutex_type>>;
	using const_lock_type = value_lock<value_type const, inner_const_lock_type>;

	constexpr base_mutex() noexcept(std::is_nothrow_default_constructible_v<value_type>) = default;
	explicit constexpr base_mutex(value_type value) noexcept(std::is_nothrow_move_constructible_v<value_type>)
		: m_value{std::move(value)} {}

	[[nodiscard]]
	auto lock() -> lock_type {
		return lock_type{m_value, m_mutex};
	}
	[[nodiscard]]
	auto lock() const -> const_lock_type {
		return const_lock();
	}
	[[nodiscard]]
	auto const_lock() const -> const_lock_type {
		return const_lock_type{m_value, m_mutex};
	}

	[[nodiscard]]
	auto try_lock() -> lock_type {
		return lock_type{m_value, m_mutex, std::try_to_lock};
	}
	[[nodiscard]]
	auto try_lock() const -> const_lock_type {
		return const_try_lock();
	}
	[[nodiscard]]
	auto const_try_lock() const -> const_lock_type {
		return const_lock_type{m_value, m_mutex, std::try_to_lock};
	}

	[[nodiscard]]
	auto defer_lock() noexcept -> lock_type {
		return lock_type{m_value, m_mutex, std::defer_lock};
	}
	[[nodiscard]]
	auto defer_lock() const noexcept -> const_lock_type {
		return const_defer_lock();
	}
	[[nodiscard]]
	auto const_defer_lock() const noexcept -> const_lock_type {
		return const_lock_type{m_value, m_mutex, std::defer_lock};
	}

	[[nodiscard]]
	auto adopt_lock() -> lock_type {
		return lock_type{m_value, m_mutex, std::adopt_lock};
	}
	[[nodiscard]]
	auto adopt_lock() const -> const_lock_type {
		return const_adopt_lock();
	}
	[[nodiscard]]
	auto const_adopt_lock() const -> const_lock_type {
		return const_lock_type{m_value, m_mutex, std::adopt_lock};
	}

	template <typename Rep, typename Period, std::enable_if_t<is_lockable_for_v<mutex_type, std::chrono::duration<Rep, Period>>, bool> = true>
	[[nodiscard]]
	auto try_lock_for(std::chrono::duration<Rep, Period> const& duration) -> lock_type {
		return lock_type{m_value, m_mutex, duration};
	}
	template <typename Rep, typename Period, std::enable_if_t<is_lockable_for_v<mutex_type, std::chrono::duration<Rep, Period>>, bool> = true>
	[[nodiscard]]
	auto try_lock_for(std::chrono::duration<Rep, Period> const& duration) const -> const_lock_type {
		return const_try_lock_for(duration);
	}
	template <typename Rep, typename Period, std::enable_if_t<is_lockable_for_v<mutex_type, std::chrono::duration<Rep, Period>>, bool> = true>
	[[nodiscard]]
	auto const_try_lock_for(std::chrono::duration<Rep, Period> const& duration) const -> const_lock_type {
		return const_lock_type{m_value, m_mutex, duration};
	}

	template <typename Clock, typename Duration, std::enable_if_t<is_lockable_until_v<mutex_type, std::chrono::time_point<Clock, Duration>>, bool> = true>
	[[nodiscard]]
	auto try_lock_until(std::chrono::time_point<Clock, Duration> const& time_point) -> lock_type {
		return lock_type{m_value, m_mutex, time_point};
	}
	template <typename Clock, typename Duration, std::enable_if_t<is_lockable_until_v<mutex_type, std::chrono::time_point<Clock, Duration>>, bool> = true>
	[[nodiscard]]
	auto try_lock_until(std::chrono::time_point<Clock, Duration> const& time_point) const -> const_lock_type {
		return const_try_lock_until(time_point);
	}
	template <typename Clock, typename Duration, std::enable_if_t<is_lockable_until_v<mutex_type, std::chrono::time_point<Clock, Duration>>, bool> = true>
	[[nodiscard]]
	auto const_try_lock_until(std::chrono::time_point<Clock, Duration> const& time_point) const -> const_lock_type {
		return const_lock_type{m_value, m_mutex, time_point};
	}

private:
	value_type         m_value;
	mutable mutex_type m_mutex{};
};

template <typename T>
class mutex: public base_mutex<T, std::mutex> {
	using base_mutex<T, std::mutex>::base_mutex;
};
template <typename T>
mutex(T value) -> mutex<T>;

template <typename T>
class shared_mutex: public base_mutex<T, std::shared_mutex> {
	using base_mutex<T, std::shared_mutex>::base_mutex;
};
template <typename T>
shared_mutex(T value) -> shared_mutex<T>;

template <typename T>
class timed_mutex: public base_mutex<T, std::timed_mutex> {
	using base_mutex<T, std::timed_mutex>::base_mutex;
};
template <typename T>
timed_mutex(T value) -> timed_mutex<T>;

template <typename T>
class shared_timed_mutex: public base_mutex<T, std::shared_timed_mutex> {
	using base_mutex<T, std::shared_timed_mutex>::base_mutex;
};
template <typename T>
shared_timed_mutex(T value) -> shared_timed_mutex<T>;
} // namespace jbd
