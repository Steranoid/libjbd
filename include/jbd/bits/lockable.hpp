#pragma once

#include "fulfill.hpp"

#include <chrono>
#include <type_traits>

namespace jbd {
struct is_basic_lockable_constraint: base_constraint {
	using base_constraint::operator();

	template <typename T>
	auto operator()(T& lock) -> std::is_void<std::void_t<decltype(lock.lock()), decltype(lock.unlock())>>;
};
template <typename T>
using is_basic_lockable = fulfill<is_basic_lockable_constraint, T&>;
template <typename T>
constexpr auto is_basic_lockable_v = is_basic_lockable<T>::value;

struct try_lock_constraint: base_constraint {
	using base_constraint::operator();
	template <typename T>
	auto operator()(T& value) -> std::is_convertible<decltype(value.try_lock()), bool>;
};
template <typename T>
using is_lockable = std::conjunction<is_basic_lockable<T>, fulfill<try_lock_constraint, T>>;
template <typename T>
constexpr auto is_lockable_v = is_lockable<T>::value;

struct try_lock_for_constraint: base_constraint {
	using base_constraint::operator();

	template <typename T, typename Rep, typename Ratio>
	auto operator()(T& value, std::chrono::duration<Rep, Ratio> const& duration) -> std::is_convertible<decltype(value.try_lock_for(duration)), bool>;
};
template <typename T, typename Duration>
using is_lockable_for = fulfill<try_lock_for_constraint, T, Duration>;
template <typename T, typename Duration>
constexpr auto is_lockable_for_v = is_lockable_for<T, Duration>::value;

struct try_lock_shared_for_constraint: base_constraint {
	using base_constraint::operator();

	template <typename T, typename Rep, typename Ratio>
	auto operator()(T& value, std::chrono::duration<Rep, Ratio> const& duration) -> std::is_convertible<decltype(value.try_lock_shared_for(duration)), bool>;
};
template <typename T, typename Duration>
using is_shared_lockable_for = fulfill<try_lock_shared_for_constraint, T, Duration>;
template <typename T, typename Duration>
constexpr auto is_shared_lockable_for_v = is_shared_lockable_for<T, Duration>::value;

struct try_lock_until_constraint: base_constraint {
	using base_constraint::operator();

	template <typename T, typename Clock, typename Duration>
	auto operator()(T& value, std::chrono::time_point<Clock, Duration> const& time_point) -> std::is_convertible<decltype(value.try_lock_until(time_point)), bool>;
};
template <typename T, typename TimePoint>
using is_lockable_until = fulfill<try_lock_until_constraint, T, TimePoint>;
template <typename T, typename TimePoint>
constexpr auto is_lockable_until_v = is_lockable_until<T, TimePoint>::value;

struct try_lock_shared_until_constraint: base_constraint {
	using base_constraint::operator();
	template <typename T, typename Clock, typename Duration>
	auto operator()(T& value, std::chrono::time_point<Clock, Duration> const& time_point) -> std::is_convertible<decltype(value.try_lock_shared_until(time_point)), bool>;
};
template <typename T, typename TimePoint>
using is_shared_lockable_until = fulfill<try_lock_shared_until_constraint, T, TimePoint>;
template <typename T, typename TimePoint>
constexpr auto is_shared_lockable_until_v = is_shared_lockable_until<T, TimePoint>::value;
} // namespace jbd