#pragma once

#include "lockable.hpp"

#include <type_traits>

namespace jbd {
template <typename T>
using is_mutex = std::conjunction<
	is_lockable<T>,
	std::is_default_constructible<T>,
	std::is_destructible<T>,
	std::negation<std::is_copy_constructible<T>>,
	std::negation<std::is_copy_assignable<T>>,
	std::negation<std::is_move_constructible<T>>,
	std::negation<std::is_move_assignable<T>>>;
template <typename T>
constexpr auto is_mutex_v = is_mutex<T>::value;
} // namespace jbd