#pragma once

#include "mutex_traits.hpp"

#include <chrono>
#include <type_traits>

namespace jbd {
struct shared_lockable_constraint: base_constraint {
	using base_constraint::operator();
	template <typename T>
	auto operator()(T& value) -> std::conjunction<
		std::is_void<decltype(value.lock_shared())>,
		std::is_void<decltype(value.unlock_shared())>,
		std::is_convertible<decltype(value.try_lock_shared()), bool>>;
};

template <typename T>
using is_shared_mutex = std::conjunction<is_mutex<T>, fulfill<shared_lockable_constraint, T&>>;

template <typename T>
constexpr auto is_shared_mutex_v = is_shared_mutex<T>::value;
} // namespace jbd