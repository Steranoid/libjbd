#pragma once

#include "jbd/bits/crtp.hpp"
#include "jbd/bits/fulfill.hpp"

#include <algorithm>
#include <functional>
#include <jbd/type_traits>
#include <memory>
#include <type_traits>
#include <utility>

namespace jbd {
struct dereferenceable_constraint: base_constraint {
	using base_constraint::operator();
	template <typename T>
	auto operator()(T&& value) -> std::is_void<std::void_t<decltype(value.operator*())>>;
};
template <typename T>
using is_dereferenceable = std::disjunction<std::is_pointer<T>, fulfill<dereferenceable_constraint, T>>;
template <typename T>
constexpr auto is_dereferenceable_v = is_dereferenceable<T>::value;

struct struct_dereferenceable_constraint: base_constraint {
	using base_constraint::operator();
	template <typename T>
	auto operator()(T&& value) -> std::is_void<std::void_t<decltype(value.operator->())>>;
};
template <typename T>
using is_struct_dereferenceable = std::disjunction<std::is_pointer<T>, fulfill<struct_dereferenceable_constraint, T>>;
template <typename T>
constexpr auto is_struct_dereferenceable_v = is_struct_dereferenceable<T>::value;

template <typename S>
struct dereferenceable_traits;
/*
{
	static auto deref(S& value) -> type;
	static auto deref(S const& value) -> type;
};
*/

template <typename T>
struct deref_type {
	using type = decltype(dereferenceable_traits<std::decay_t<T>>::deref(std::declval<T>()));
};
template <typename T>
using deref_type_t = typename deref_type<T>::type;

struct deref_constraint {
	template <typename T>
	auto operator()(T&& value) -> std::is_void<std::void_t<decltype(dereferenceable_traits<std::decay_t<T>>::deref(std::forward<T&&>(value)))>>;
};

template <typename T>
auto deref(T&& value) noexcept(noexcept(dereferenceable_traits<std::decay_t<T>>::deref(std::forward<T&&>(value)))) -> decltype(auto) {
	return dereferenceable_traits<std::decay_t<T>>::deref(std::forward<T&&>(value));
}

template <typename S>
struct dereferenceable: crtp<S, dereferenceable<S>> {
	auto operator*() & noexcept(noexcept(deref(this->self()))) -> decltype(auto) {
		return dereferenceable_traits<S>::deref(this->self());
	}
	auto operator*() && noexcept(noexcept(deref(this->self_into()))) -> decltype(auto) {
		return dereferenceable_traits<S>::deref(this->self_into());
	}
	auto operator*() const& noexcept(noexcept(deref(this->self()))) -> decltype(auto) {
		return dereferenceable_traits<S>::deref(this->self());
	}
	auto operator*() const&& noexcept(noexcept(deref(this->self_into()))) -> decltype(auto) {
		return dereferenceable_traits<S>::deref(this->self_into());
	}

	template <typename... Args>
	auto operator()(Args&&... args) & noexcept(noexcept(std::invoke(*this->self(), std::forward<Args>(args)...))) -> decltype(auto) {
		return std::invoke(*this->self(), std::forward<Args>(args)...);
	}
	template <typename... Args>
	auto operator()(Args&&... args) && noexcept(noexcept(std::invoke(*this->self_into(), std::forward<Args>(args)...))) -> decltype(auto) {
		return std::invoke(*this->self_into(), std::forward<Args>(args)...);
	}
	template <typename... Args>
	auto operator()(Args&&... args) const& noexcept(noexcept(std::invoke(*this->self(), std::forward<Args>(args)...))) -> decltype(auto) {
		return std::invoke(*this->self(), std::forward<Args>(args)...);
	}
	template <typename... Args>
	auto operator()(Args&&... args) const&& noexcept(noexcept(std::invoke(*this->self_into(), std::forward<Args>(args)...))) -> decltype(auto) {
		return std::invoke(*this->self_into(), std::forward<Args>(args)...);
	}

	auto operator->() & noexcept(noexcept(*this->self())) -> decltype(auto) {
		decltype(auto) deref = *this->self();
		if constexpr (is_struct_dereferenceable_v<decltype(deref)>) {
			return std::forward<decltype(deref)>(deref);
		} else {
			return std::addressof(deref);
		}
	}
	auto operator->() && noexcept(noexcept(*this->self_into())) -> decltype(auto) {
		decltype(auto) deref = *this->self_into();
		if constexpr (is_struct_dereferenceable_v<decltype(deref)>) {
			return std::forward<decltype(deref)>(deref);
		} else {
			return std::addressof(deref);
		}
	}
	auto operator->() const& noexcept(noexcept(*this->self())) -> decltype(auto) {
		decltype(auto) deref = *this->self();
		if constexpr (is_struct_dereferenceable_v<decltype(deref)>) {
			return std::forward<decltype(deref)>(deref);
		} else {
			return std::addressof(deref);
		}
	}
	auto operator->() const&& noexcept(noexcept(*this->self_into())) -> decltype(auto) {
		decltype(auto) deref = *this->self_into();
		if constexpr (is_struct_dereferenceable_v<decltype(deref)>) {
			return std::forward<decltype(deref)>(deref);
		} else {
			return std::addressof(deref);
		}
	}
};
} // namespace jbd