#pragma once

#include "jbd/bits/deref.hpp"

#include <memory>

namespace jbd {
template <typename T>
class option;

template <typename T>
class ref: public dereferenceable<ref<T>> {
public:
	using reference = T&;
	using pointer   = T*;

	explicit ref(reference value)
		: ref{std::addressof(value)} {}

private:
	friend dereferenceable_traits<ref>;
	friend option<ref>;

	explicit ref(pointer value)
		: m_value{value} {}

	pointer m_value;
};

template <typename T>
struct dereferenceable_traits<ref<T>> {
	static auto deref(ref<T> const& reference) noexcept -> typename ref<T>::reference {
		return *reference.m_value;
	}
};
} // namespace jbd