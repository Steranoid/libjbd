#pragma once

#include "jbd/bits/deref.hpp"
#include "jbd/bits/mutex_traits.hpp"

#include <chrono>
#include <functional>
#include <jbd/ref>
#include <jbd/type_traits>
#include <mutex>
#include <shared_mutex>
#include <type_traits>
#include <utility>

namespace jbd {
template <typename T, typename L>
class value_lock: public dereferenceable<value_lock<T, L>> {
public:
	using value_type = T;
	using lock_type  = L;
	using mutex_type = typename lock_type::mutex_type;
	using pointer    = std::add_pointer_t<T>;
	using reference  = std::add_lvalue_reference_t<T>;

	static_assert(!std::is_reference_v<lock_type>);
	static_assert(!std::is_reference_v<value_type>);
	static_assert(!std::is_const_v<lock_type>);
	static_assert(is_lockable_v<lock_type>);
	static_assert(std::is_constructible_v<bool, lock_type>);

	value_lock() = delete;

	value_lock(value_lock const& other)                    = delete;
	auto operator=(value_lock const& other) -> value_lock& = delete;

	value_lock(value_lock&& other) noexcept                    = default;
	auto operator=(value_lock&& other) noexcept -> value_lock& = default;

	~value_lock() noexcept = default;

	template <
		typename U,
		typename J                                             = lock_type,
		std::enable_if_t<!std::is_same_v<U, value_lock>, bool> = true,
		std::enable_if_t<is_lockable_v<J>, bool>               = true,
		std::enable_if_t<!is_mutex_v<J>, bool>                 = true>
	explicit value_lock(U&& value, J&& lock = J{}) noexcept
		: m_value{std::forward<U>(value)}
		, m_lock{std::forward<J>(lock)} {}
	template <typename U, std::enable_if_t<!std::is_same_v<U, value_lock>, bool> = true>
	value_lock(U&& value, mutex_type& mutex)
		: value_lock{std::forward<U>(value), lock_type{mutex}} {}

	template <typename U, std::enable_if_t<!std::is_same_v<U, value_lock>, bool> = true>
	value_lock(U&& value, mutex_type& mutex, std::try_to_lock_t flag)
		: value_lock{
			std::forward<U>(value),
			lock_type{mutex, flag}
        } {}
	template <typename U, std::enable_if_t<!std::is_same_v<U, value_lock>, bool> = true>
	value_lock(U&& value, mutex_type& mutex, std::defer_lock_t flag) noexcept
		: value_lock{
			std::forward<U>(value),
			lock_type{mutex, flag}
        } {}
	template <typename U, std::enable_if_t<!std::is_same_v<U, value_lock>, bool> = true>
	value_lock(U&& value, mutex_type& mutex, std::adopt_lock_t flag)
		: value_lock{
			std::forward<U>(value),
			lock_type{mutex, flag}
        } {}

	template <
		typename U,
		typename Rep,
		typename Period,
		typename Mutex                                                                       = mutex_type,
		std::enable_if_t<is_lockable_for_v<Mutex, std::chrono::duration<Rep, Period>>, bool> = true,
		std::enable_if_t<!std::is_same_v<std::decay_t<U>, std::decay_t<value_lock>>, bool>   = true>
	value_lock(U&& value, mutex_type& mutex, std::chrono::duration<Rep, Period> const& duration)
		: value_lock{
			std::forward<U>(value),
			lock_type{mutex, duration}
        } {}
	template <
		typename U,
		typename Clock,
		typename Duration,
		typename Mutex                                                                               = mutex_type,
		std::enable_if_t<is_lockable_until_v<Mutex, std::chrono::time_point<Clock, Duration>>, bool> = true,
		std::enable_if_t<!std::is_same_v<std::decay_t<U>, std::decay_t<value_lock>>, bool>           = true>
	value_lock(U&& value, mutex_type& mutex, std::chrono::time_point<Clock, Duration> const& time_point)
		: value_lock{
			std::forward<U>(value),
			lock_type{mutex, time_point}
        } {}

	auto lock() -> value_lock& {
		m_lock.lock();
		return *this;
	}
	auto unlock() -> value_lock& {
		m_lock.unlock();
		return *this;
	}
	auto try_lock() -> value_lock& {
		m_lock.try_lock();
		return *this;
	}

	template <
		typename Rep,
		typename Period,
		typename Mutex                                                                       = mutex_type,
		std::enable_if_t<is_lockable_for_v<Mutex, std::chrono::duration<Rep, Period>>, bool> = true>
	auto try_lock_for(std::chrono::duration<Rep, Period> const& duration) -> value_lock& {
		m_lock.try_lock_for(duration);
		return *this;
	}
	template <
		typename Clock,
		typename Duration,
		typename Mutex                                                                               = mutex_type,
		std::enable_if_t<is_lockable_until_v<Mutex, std::chrono::time_point<Clock, Duration>>, bool> = true>
	auto try_lock_until(std::chrono::time_point<Clock, Duration> const& time_point) -> value_lock& {
		m_lock.try_lock_until(time_point);
		return *this;
	}

	[[nodiscard]]
	auto owns_lock() const noexcept -> bool {
		return static_cast<bool>(m_lock);
	}
	explicit operator bool() const noexcept { return owns_lock(); }

	[[nodiscard]]
	auto value() const -> reference {
		if (!owns_lock()) {
			m_lock.lock();
		}
		return *m_value;
	}

private:
	jbd::ref<T>       m_value;
	mutable lock_type m_lock;
};

template <typename T, typename L>
struct dereferenceable_traits<value_lock<T, L>> {
	static auto deref(value_lock<T, L> const& value) -> typename value_lock<T, L>::reference {
		return value.value();
	}
};

template <
	typename T,
	typename L,
	std::enable_if_t<!is_mutex_v<L>, bool> = true>
value_lock(T&& value, L lock) -> value_lock<std::remove_reference_t<T>, L>;
template <typename T, typename M, typename... Args, std::enable_if_t<is_mutex_v<M>, bool> = true>
value_lock(T&& value, M& mutex, Args&&... args) -> value_lock<std::remove_reference_t<T>, std::conditional_t<is_shared_mutex_v<M>, std::shared_lock<M>, std::unique_lock<M>>>;
} // namespace jbd