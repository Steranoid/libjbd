#pragma once

#include "jbd/bits/deref.hpp"
#include "jbd/bits/ref.hpp"

#include <type_traits>
#include <utility>

namespace jbd {

struct none_t {};
constexpr auto none = none_t{};

template <typename T>
class option: public dereferenceable<option<T>> {
public:
	using value_type = T;
	option()
		: option{none} {}
	explicit option(none_t value)
		: m_state{state_type::none}
		, m_none{value} {}
	auto operator=([[maybe_unused]] none_t value) -> option& {
		reset();
		return *this;
	}

	explicit option(value_type value)
		: m_state{state_type::some}
		, m_some{std::forward<value_type&&>(value)} {}
	auto operator=(value_type value) -> option& {
		emplace(std::forward<value_type&&>(value));
		return *this;
	}

	option(option const& other)
		: option{none} {
		*this = other;
	}
	auto operator=(option const& other) -> option& {
		assert(&other != this);
		switch (other.m_state) {
		case state_type::none:
			*this = none;
			break;
		case state_type::some:
			*this = other.m_some;
			break;
		}
		return *this;
	}

	option(option&& other) noexcept
		: option{none} {
		*this = std::move(other);
	}
	auto operator=(option&& other) noexcept -> option& {
		assert(&other != this);
		switch (other.m_state) {
		case state_type::none:
			*this = none;
			break;
		case state_type::some:
			*this = std::forward<value_type>(other.m_some);
			break;
		}
		return *this;
	}

	~option() noexcept {
		reset();
	}

	[[nodiscard]]
	auto is_some() const noexcept -> bool {
		return m_state == state_type::some;
	}
	[[nodiscard]]
	auto is_none() const noexcept -> bool {
		return m_state == state_type::none;
	}
	explicit operator bool() const noexcept {
		return is_some();
	}

	template <typename... Args>
	auto emplace(Args&&... args) -> value_type& {
		reset();
		m_state = state_type::some;
		new (&m_some) value_type{std::forward<Args>(args)...}; // NOLINT(cppcoreguidelines-pro-type-union-access)
		return m_some;                                         // NOLINT(cppcoreguidelines-pro-type-union-access)
	}

	auto reset() noexcept -> option& {
		switch (std::exchange(m_state, state_type::none)) {
		case state_type::none:
			break;
		case state_type::some:
			m_some.~value_type(); // NOLINT(cppcoreguidelines-pro-type-union-access)
			break;
		}
		return *this;
	}

private:
	friend dereferenceable_traits<option>;

	enum class state_type {
		none,
		some,
	};
	state_type m_state;
	union {
		none_t     m_none;
		value_type m_some;
	};
};

template <typename T>
class option<ref<T>>: public dereferenceable<option<ref<T>>> {
public:
	using value_type = ref<T>;
	option()
		: option{none} {}
	explicit option([[maybe_unused]] none_t value)
		: m_value{nullptr} {}
	auto operator=([[maybe_unused]] none_t value) -> option& {
		reset();
		return *this;
	}

	explicit option(T& value)
		: m_value{value} {}
	auto operator=(T& value) -> option& {
		emplace(std::forward<value_type&&>(value));
		return *this;
	}

	[[nodiscard]]
	auto is_some() const noexcept -> bool {
		return m_value.m_value;
	}
	[[nodiscard]]
	auto is_none() const noexcept -> bool {
		return !is_some();
	}
	explicit operator bool() const noexcept {
		return is_some();
	}

	template <typename... Args>
	auto emplace(Args&&... args) -> value_type& {
		m_value = ref{std::forward<Args>(args)...};
		return m_value;
	}

	auto reset() noexcept -> option& {
		m_value.m_value = nullptr;
		return *this;
	}

private:
	friend dereferenceable_traits<option>;

	value_type m_value;
};

template <typename T>
option(T& value) -> option<ref<T>>;
template <typename T>
option(T&& value) -> option<T>;

template <typename T>
struct dereferenceable_traits<option<T>> {
	static auto deref(option<T>& value) -> T& {
		return value.m_some;
	}
	static auto deref(option<T> const& value) -> T const& {
		return value.m_some;
	}
	static auto deref(option<T>&& value) -> T&& {
		return std::move(value.m_some);
	}
	static auto deref(option<T> const&& value) -> T const&& {
		return std::move(value.m_some);
	}
};

template <typename T>
struct dereferenceable_traits<option<jbd::ref<T>>> {
	static auto deref(option<ref<T>>& value) -> T& {
		return *value.m_value;
	}
	static auto deref(option<ref<T>> const& value) -> T const& {
		return *value.m_value;
	}
};

template <typename T>
auto operator==(option<T> const& left, none_t /*unused*/) noexcept -> bool {
	return left.is_none();
}
template <typename T>
auto operator==(none_t /*unused*/, option<T> const& right) noexcept -> bool {
	return right.is_none();
}
template <typename T>
auto operator!=(option<T> const& left, none_t right) noexcept -> bool {
	return !(left == right);
}
template <typename T>
auto operator!=(none_t left, option<T> const& right) noexcept -> bool {
	return !(left == right);
}
template <typename T, typename U>
auto operator==(option<T> const& left, U const& right) -> bool {
	return left.is_some() && *left == right;
}
template <typename T, typename U>
auto operator==(T const& left, option<U> const& right) -> bool {
	return right == left;
}
template <typename T, typename U>
auto operator==(option<T> const& left, option<U> const& right) -> bool {
	return (left.is_none() && right.is_none()) || (right.is_some() && left == *right);
}
template <typename T, typename U>
auto operator!=(option<T> const& left, U const& right) -> bool {
	return !(left == right);
}
template <typename T, typename U>
auto operator!=(T const& left, option<U> const& right) -> bool {
	return !(left == right);
}
template <typename T, typename U>
auto operator!=(option<T> const& left, option<U> const& right) -> bool {
	return !(left == right);
}
} // namespace jbd