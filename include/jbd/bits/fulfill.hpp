#pragma once

#include <type_traits>

namespace jbd {
struct base_constraint {
	auto operator()(...) -> std::false_type;
};

template <typename Constraint, typename... Args>
struct fulfill {
	using value_type = bool;
	using type       = decltype(std::declval<Constraint>()(std::declval<Args&>()...)); // NOLINT(cppcoreguidelines-pro-type-vararg,hicpp-vararg)

	static constexpr auto value = type::value;

	constexpr explicit operator bool() const { return value; }
	constexpr auto operator()() const -> bool { return value; }
};

template <typename Constraint, typename... Args>
constexpr auto fulfill_v = fulfill<Constraint, Args...>::value;
} // namespace jbd