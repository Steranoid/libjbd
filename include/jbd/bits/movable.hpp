#pragma once

#include <type_traits>

namespace jbd {
template <typename T>
using is_movable = std::conjunction<std::is_move_constructible<T>, std::is_move_assignable<T>>;

template <typename T>
constexpr auto is_movable_v = is_movable<T>::value;
} // namespace jbd