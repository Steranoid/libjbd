#pragma once

#include <type_traits>
namespace jbd {
template <template <typename...> typename T, typename U>
struct is_template: std::false_type {};

template <template <typename...> typename T, typename... Args>
struct is_template<T, T<Args...>>: std::true_type {};

template <template <typename...> typename T, typename U>
constexpr auto is_template_v = is_template<T, U>::value;
} // namespace jbd