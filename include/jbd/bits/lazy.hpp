#pragma once

#include "jbd/bits/deref.hpp"

#include <mutex>
#include <type_traits>
#include <variant>

namespace jbd {
template <typename Fn>
class lazy: public dereferenceable<lazy<Fn>> {
public:
	using function_type = Fn;
	using value_type    = std::invoke_result_t<function_type>;
	using reference     = std::add_lvalue_reference_t<std::add_const_t<value_type>>;
	using pointer       = std::add_pointer_t<std::add_const_t<value_type>>;
	using deref_type    = std::conditional_t<std::is_trivial_v<value_type>, value_type, reference>;

	explicit lazy(function_type&& function) noexcept(std::is_nothrow_constructible_v<variant_type, function_type&&>)
		: m_value{std::move(function)} {}
	explicit lazy(function_type& function) noexcept(std::is_nothrow_constructible_v<variant_type, function_type&>)
		: m_value{function} {}

	[[nodiscard]]
	auto value() const noexcept(std::is_nothrow_invocable_v<function_type>) -> deref_type {
		std::call_once(m_flag, [&] {
			m_value = std::get<function_type>(m_value)();
		});
		return std::get<value_type>(m_value);
	}

private:
	using variant_type = std::variant<function_type, value_type>;

	mutable variant_type   m_value;
	mutable std::once_flag m_flag{};
};

template <typename Fn>
struct dereferenceable_traits<lazy<Fn>> {
	static auto deref(lazy<Fn> const& value) noexcept(std::is_nothrow_invocable_v<Fn>) -> typename lazy<Fn>::deref_type {
		return value.value();
	}
};
} // namespace jbd
