#pragma once

namespace jbd {
template <typename S, typename T>
struct crtp {
	using self_type  = S;
	using trait_type = T;
	auto self() noexcept -> self_type& {
		return static_cast<self_type&>(*this);
	}
	auto self() const noexcept -> self_type const& {
		return const_self();
	}
	auto const_self() const noexcept -> self_type const& {
		return static_cast<self_type const&>(*this);
	}
	auto self_into() noexcept -> self_type&& {
		return static_cast<self_type&&>(*this);
	}
	auto self_into() const noexcept -> self_type const&& {
		return const_self_into();
	}
	auto const_self_into() const noexcept -> self_type const&& {
		return static_cast<self_type const&&>(*this);
	}
};
} // namespace jbd