#pragma once

#include <type_traits>

namespace jbd {
template <typename T>
using is_copyable = std::conjunction<std::is_copy_constructible<T>, std::is_copy_assignable<T>>;

template <typename T>
constexpr auto is_copyable_v = is_copyable<T>::value;
} // namespace jbd