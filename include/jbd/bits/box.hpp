#pragma once

#include "jbd/bits/deref.hpp"
#include "jbd/bits/option.hpp"

#include <jbd/ref>
#include <memory>
#include <utility>

namespace jbd {
template <typename T, typename D = std::default_delete<T>>
class box: public dereferenceable<box<T>> {
public:
	using value_type      = T;
	using pointer         = value_type*;
	using reference       = value_type&;
	using const_reference = value_type const&;
	using deleter_type    = D;

	box() = delete;
	explicit box(value_type value)
		: box{std::move(value), deleter_type{}} {}
	box(value_type value, deleter_type deleter)
		: box{new value_type{std::move(value)}, std::move(deleter)} {}

	box(box const& other)                    = delete;
	auto operator=(box const& other) -> box& = delete;

	box(box&& other) noexcept
		: box{std::exchange(other.m_value, nullptr), std::move(other.m_deleter)} {}
	auto operator=(box&& other) noexcept -> box& {
		assert(std::addressof(other) != this);
		reset();
		std::swap(m_value, other.m_value);
		return *this;
	}
	~box() {
		reset();
	}

private:
	friend dereferenceable_traits<box>;
	friend option<box>;

	box(pointer value, deleter_type deleter)
		: m_value{value}
		, m_deleter{std::move(deleter)} {}

	auto reset() -> box& {
		m_deleter(std::exchange(m_value, nullptr));
		return *this;
	}

	pointer      m_value;
	deleter_type m_deleter;
};

template <typename T>
struct dereferenceable_traits<box<T>> {
	static auto deref(box<T> const& value) noexcept -> typename box<T>::const_reference {
		return *value.m_value;
	}
	static auto deref(box<T>& value) noexcept -> typename box<T>::reference {
		return *value.m_value;
	}
};

template <typename T, typename D>
class option<box<T, D>>: public dereferenceable<option<box<T, D>>> {
public:
	using value_type = box<T, D>;

	option()
		: option{none} {}
	explicit option([[maybe_unused]] none_t value)
		: m_value{nullptr, D{}} {}
	auto operator=([[maybe_unused]] none_t value) -> option& {
		reset();
		return *this;
	}
	explicit option(value_type value)
		: m_value{std::move(value)} {}
	auto operator=(value_type value) -> option& {
		emplace(std::move(value));
		return *this;
	}

	[[nodiscard]]
	auto is_some() const noexcept -> bool {
		return m_value.m_value;
	}
	[[nodiscard]]
	auto is_none() const noexcept -> bool {
		return !is_some();
	}
	explicit operator bool() const noexcept {
		return is_some();
	}

	template <typename... Args>
	auto emplace(Args&&... args) -> value_type& {
		reset();
		new (&m_value) value_type{std::forward<Args>(args)...};
		return m_value;
	}
	auto reset() -> option& {
		m_value.reset();
		return *this;
	}

private:
	friend dereferenceable_traits<option>;

	value_type m_value;
};

template <typename T>
struct dereferenceable_traits<option<box<T>>> {
	static auto deref(option<box<T>>& value) -> box<T>& {
		return value.m_value;
	}
	static auto deref(option<box<T>> const& value) -> box<T> const& {
		return value.m_value;
	}
	static auto deref(option<box<T>>&& value) -> box<T>&& {
		return std::move(value.m_value);
	}
	static auto deref(option<box<T>> const&& value) -> box<T> const&& {
		return std::move(value.m_value);
	}
};
} // namespace jbd